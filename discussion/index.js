//[SECTION] Data Modeling

	//1. Identify what infomation we want to gather from the customers in order to determine wether the user's identity is true.
		//Why need to identify first what information want?
		//- To properly plan out what infromation will be deemed useful.
		//- To lessen the chances or scenarios of having to modify or edit the data stored in the database
		//- To anticipate how this data/information would relate to each other.

	//TASK: Create a Course Booking System for an Institution

		//What Are the minimum information would i need to collect from the customers/student
		//The following information described below would identify the structure of the user in our app

		//User Documents:
		// 1. first name
		// 2. last name
		// 3. middle name
		// 4. email address
		// 5. pin/password
		// 6. mobile number
		// 7. birthdate
		// 8. gender
		// 9. age
		// 10. isAdmin => role and restriction/limitation that this user would have in our app.
		//11. dateTimeRegistered => when the student signed up/ enrolled in the institution

		//Course/Subjects
			//1. name/title
			//2. course code
			//3. course description 
			//4. course units
			//5. course instructor
			//6. isActive => to describe if the course is being offered by institution
			//7. dateTimeCreated => for us to identify when the course was added to the database
			//8. available slots





			//As a full stack web developer (front, backend, database)
			//The more information you gather the more difficult it is to scale and manage the collection, it is going more difficult to maintain

	//2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them.

		//users can have multiple subjects
		//subjects can have multiple enrollees

		//user can have multiple transactions
		//transaction can only belong to a single user

		//a single transaction can have multiple courses 
		//a course can be part of multiple transactions

	//3. Convert and Translate the EDR into a JSON-like syntax in order to describe the structure of the documents inside the collection by creating

		//NOTE: Creating/using mock data can play an integral role during the testing phase of any app development

		//username: dahsggshamnf -> this is one of the things you need to avoid.

		//data => raw no context